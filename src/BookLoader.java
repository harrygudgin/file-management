
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

@SuppressWarnings("ALL")

public class BookLoader {
    public static void main(String[] args) {
        try {
            String csvFile = "data/books.csv";

            FileReader fileReader = new FileReader(csvFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            Book[] books = new Book[10];

            String nextLine = bufferedReader.readLine();

            for (int i = 0; i < books.length; i++) {
                nextLine = bufferedReader.readLine();

                if (nextLine != null) {
                    String[] strings = nextLine.split(",");

                    String title = strings[0];
                    String author = strings[1];
                    String date = strings[2];

                    books[i] = new Book(title, author, date);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
            System.exit(1);
        }
    }
}