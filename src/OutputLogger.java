
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class OutputLogger
{
    public static void main(String[] args)
    {
        try {
            boolean append = true;
            FileHandler handler = new FileHandler("default.log", append);
            handler.setFormatter(new SimpleFormatter());

            Logger logger = Logger.getLogger("test_output_logger");
            logger.addHandler(handler);

            logger.info("info message");
            logger.warning("warning message");
            logger.severe("severe message");
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
            System.exit(1);
        }
    }
}
