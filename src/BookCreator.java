import java.io.FileWriter;

public class BookCreator
{
    public static void main(String[] args)
    {
        Book[] newBooks = new Book[3];
        newBooks[0] = new Book("The Hobbit", "J. R. R. Tolkien", "21 September 1937");
        newBooks[1] = new Book("Nineteen Eighty-Four", "George Orwell", "8 June 1949");
        newBooks[2] = new Book("Pride and Prejudice", "Jane Austen", "28 January 1813");

        try
        {
            FileWriter csvWriter = new FileWriter("data/books.csv");

            csvWriter.append("Title");
            csvWriter.append(",");
            csvWriter.append("Author");
            csvWriter.append(",");
            csvWriter.append("Date");
            csvWriter.append("\n");

            for (int i = 0; i < newBooks.length; i++)
            {
                Book newBook = newBooks[i];

                csvWriter.append(newBooks[i].getTitle());
                csvWriter.append(",");
                csvWriter.append(newBooks[i].getAuthor());
                csvWriter.append(",");
                csvWriter.append(newBooks[i].getDate());
                csvWriter.append("\n");
            }

            csvWriter.flush();
            csvWriter.close();

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
            System.exit(1);
        }
    }
}